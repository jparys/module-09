import logging
import time
import azure.functions as func

import uuid
import json
import typing


def main(request: func.HttpRequest, message: func.Out[str], msg: func.Out[typing.List[str]]) -> func.HttpResponse:
    logging.info('HTTP trigger function received a request.')
    start = time.time()

    req_body = request.get_json()
    subtitle = req_body.get("subtitle")

    end = time.time()
    processingTime = end - start

    rowKey = str(uuid.uuid4())

    data = {
        "subtitle": subtitle,
        "PartitionKey": "message",
        "rowKey": rowKey
    }
    message.set(json.dumps(data))

    languages = req_body.get("languages")
    translations = []
    for languageCode in languages:

        translation  = {
            "rowKey" : rowKey,
            "languageCode": languageCode
            }
        translations.append(json.dumps(translation))
        
    msg.set(translations)

    #msg.set(json.dumps(translation))
    return func.HttpResponse(
        f"Processing took {str(processingTime)} seconds. Translation is: {subtitle}, languages : {languages}",
        status_code=200
    )
