import logging

import azure.functions as func

import uuid
import json
import typing



def main(msg: func.QueueMessage, translationData, translation: func.Out[str] ) -> None:
    logging.info('>>> : %s', msg.get_body().decode('utf-8'))
    language = json.loads( msg.get_body().decode('utf-8')).get("languageCode") 
    message = json.loads(translationData)
    #return func.HttpResponse(f"Table row: {messageJSON}")
    logging.info(translationData)
    logging.info(message)
    rowKey = str(uuid.uuid4())
    data = {
        "translation": message.get("subtitle").upper(),
        "language": language,
        "rowKey": rowKey
    }
    translation.set(json.dumps(data))
